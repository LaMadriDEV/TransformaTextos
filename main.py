import sys
from Vista import VentanaPrincipal
from PyQt5 import Qt, QtGui, QtCore


if __name__ == '__main__':
    app = Qt.QApplication(sys.argv)
    w = VentanaPrincipal()
    w.show()
    sys.exit(app.exec_())
