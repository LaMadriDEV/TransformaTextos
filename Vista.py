from UI import Ventana
from PyQt5 import Qt


class VentanaPrincipal(Qt.QMainWindow):
    def __init__(self):
        super(VentanaPrincipal, self).__init__()
        self.ui = Ventana.Ui_VentanaPrincipal()

        self.ui.setupUi(self)

        self.ui.boton_tabulaciones.clicked.connect(self.quitar_tabulaciones)
        self.ui.boton_intercambio.clicked.connect(self.aplicar_intercambio)
        self.ui.boton_csv.clicked.connect(self.aplicar_csv)
        self.ui.boton_nada.clicked.connect(self.la_nada_misma)

    def aplicar_intercambio(self):
        if self.ui.cambiar.text() != "":
            texto = self.ui.texto_a_transformar.toPlainText()
            valor_de = self.ui.cambiar.text()
            valor_a = self.ui.por.text()
            self.ui.texto_transformado.setText(texto.replace(str(valor_de), str(valor_a)))

    def quitar_tabulaciones(self):
        texto = self.ui.texto_a_transformar.toPlainText()
        sin_saltos = texto.replace("\n", "")
        nuevo_texto = sin_saltos.replace("\t", "")
        self.ui.texto_transformado.setText(nuevo_texto)

    def aplicar_csv(self):
        if self.ui.separador.text() != "":
            texto = self.ui.texto_a_transformar.toPlainText()
            self.ui.texto_transformado.setText(texto.replace(" ", str(self.ui.separador.text())))

    def la_nada_misma(self):
        self.ui.texto_transformado.setText("Nada Robertita!\n\n\nBy: Roberto")
